const { v4 } = require('uuid');
const express = require('express');
const format = require('pg-format');
const { query } = require('../../utils/postgres.js');
const auth = require('../../middleware/auth');

const router = express.Router();

//@route   POST /api/groups/createGroup
//@desc    Create a new Group
//access   Private

router.post('/createGroup', auth, async (req, res) => {
    let { name } = req.body;
    try {

        // Check if Name is not blank
        if (!name?.trim()) {
            return res.status(403).json({ errors: [{ message: 'Name cannot be blank' }] });
        }

        const id = v4();

        // Create the new group 
        const createGroupQuery = format('INSERT INTO groups(id, name, creator_id) VALUES(%L)', [id, name, req.id]);
        await query(createGroupQuery);
        // Insert creator as member in group
        const insertMemberQuery = format('INSERT INTO group_members(group_id, member_id) VALUES(%L)', [id, req.id]);
        await query(insertMemberQuery);

        return res.status(200).send({ message: "Successfully created group", groupId: id });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to create group. Please try again later.' }] });
    }
});

//@route   GET /api/groups/fetchUserGroups
//@desc    Fetch the groups of a user
//access   Private

router.get('/fetchUserGroups', auth, async (req, res) => {
    try {
        // Select groups the user is part of 
        const fetchGroupsQuery = format('SELECT g.id, g.name from group_members gm JOIN groups g ON gm.group_id = g.id WHERE gm.member_id = %L', req.id);
        const { rows: groups } = await query(fetchGroupsQuery);
        return res.status(200).send({ message: "Successfully fetched groups", groups });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to create group. Please try again later.' }] });
    }
});

//@route   POST /api/groups/addMembersToGroup
//@desc    Add members to a group
//access   Private

router.post('/addMembersToGroup', auth, async (req, res) => {
    let { members, groupId } = req.body;
    try {

        // Check if groupID is not blank
        if (!groupId?.trim()) {
            return res.status(403).json({ errors: [{ message: 'GroupID cannot be blank' }] });
        }

        // Check if members array is empty
        if (members?.length === 0 || !Array.isArray(members)) {
            return res.status(403).json({ errors: [{ message: 'No members to add, send array of numbers of members to add' }] });
        }

        // Fetch IDs of members from number
        const fetchIdQuery = format("SELECT name, id FROM users u WHERE u.phone in (%L)", members);
        const { rows } = await query(fetchIdQuery);

        let newMembersDetails = [];

        for (let row of rows) {
            const { name, id } = row;
            newMembersDetails.push({ name, id })
            const insertMemberQuery = format('INSERT INTO group_members(group_id, member_id) VALUES(%L) ON CONFLICT DO NOTHING', [groupId, id]);
            await query(insertMemberQuery);
        }

        return res.status(200).send({ message: "Successfully inserted members into group", newMembersDetails });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to insert members. Please try again later.' }] });
    }
});

//@route   GET /api/groups/loadGroupMembers
//@desc    Load members of a group
//access   Private

router.get('/loadGroupMembers/:groupId', auth, async (req, res) => {
    try {
        const groupId = req.params?.groupId;

        // Check if groupID is not blank
        if (!groupId?.trim()) {
            return res.status(403).json({ errors: [{ message: 'GroupID cannot be blank' }] });
        }

        // Fetch IDs of members from number
        const fetchIdQuery = format("SELECT u.id, u.name FROM users u JOIN group_members g ON u.id = g.member_id WHERE g.group_id = %L", groupId);
        const { rows } = await query(fetchIdQuery);

        let members = []

        for (let row of rows) {
            const { name, id } = row;
            members.push({ name, id });
        }

        return res.status(200).send({ members });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to load members. Please try again later.' }] });
    }
});

//@route   GET /api/groups/loadGroupInfo/:groupId
//@desc    Load members of a group
//access   Private

router.get('/loadGroupInfo/:groupId', auth, async (req, res) => {
    try {
        const groupId = req.params?.groupId;

        // Check if groupID is not blank
        if (!groupId?.trim()) {
            return res.status(403).json({ errors: [{ message: 'GroupID cannot be blank' }] });
        }

        // Fetch IDs of members from number
        const fetchIdQuery = format("SELECT u.id, u.name FROM users u JOIN group_members g ON u.id = g.member_id WHERE g.group_id = %L", groupId);
        const { rows } = await query(fetchIdQuery);

        let owingDetails = [];
        let members = []

        for (let row of rows) {
            const { name, id } = row;
            members.push({ name, id });
            if (id != req.id) {
                const getPaymentDetails = format(
                    `
                    SELECT a.val - b.val as diff FROM
                        (SELECT coalesce((SELECT SUM(t.amount) FROM transactions t WHERE t.payer = %L AND payee = %L GROUP BY t.payee),0) as val) a CROSS JOIN
                        (SELECT coalesce((SELECT SUM(t.amount) FROM transactions t WHERE t.payee = %L AND payer = %L GROUP BY t.payee),0) as val) b
                    ;
                    `, req.id, id, req.id, id
                );
                const { rows: paymentRows } = await query(getPaymentDetails);
                if (paymentRows.length > 0) {
                    const amount = paymentRows[0].diff;
                    if (amount < 0) {
                        owingDetails.push({ name, type: 'owe', amount: Math.abs(amount), id })
                    } else if (amount > 0) {
                        owingDetails.push({ name, type: 'owed', amount, id })
                    }
                }
            }
        }

        let bills = []
        const fetchBillsQuery = format('SELECT b.amount, b.id, u.name from bills b JOIN users u on u.id = b.payer WHERE b.group_id = %L', groupId);
        const { rows: billRows } = await query(fetchBillsQuery);

        for (let bill of billRows) {
            bills.push({ amount: bill.amount, payer: bill.name, id: bill.id })
        }

        return res.status(200).send({ bills, owingDetails, members });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to load members. Please try again later.' }] });
    }
});

module.exports = router;