const { v4 } = require('uuid');
const express = require('express');
const format = require('pg-format');
const { query } = require('../../utils/postgres.js');
const auth = require('../../middleware/auth');

const router = express.Router();

//@route   POST /api/bills/createBill
//@desc    Create a new Bill
//access   Private

router.post('/createBill', auth, async (req, res) => {
    let { groupId, amount, split, members, payer } = req.body;
    try {
        if (!groupId?.trim()) {
            return res.status(403).json({ errors: [{ message: 'GroupID cannot be blank' }] });
        }
        if (!amount) {
            return res.status(403).json({ errors: [{ message: 'Amount cannot be blank' }] });
        }
        if (!payer?.trim()) {
            return res.status(403).json({ errors: [{ message: 'Payer cannot be blank' }] });
        }
        if (!members?.length) {
            return res.status(403).json({ errors: [{ message: 'Members cannot be blank' }] });
        }
        if (!split?.trim()) {
            split = 'equal'
        }

        const billId = v4();

        const billInsertQuery = format('INSERT INTO bills(id, group_id, creator_id, amount, payer) VALUES(%L)', [billId, groupId, req.id, amount, payer]);
        await query(billInsertQuery);

        if (split === 'equal') {
            const userAmount = amount / members.length;
            for (let member of members) {
                if (String(member) !== String(payer)) {
                    const t_id = v4();
                    const transactionInsertQuery = format('INSERT INTO transactions(id, group_id, bill_id, payer, payee, amount) VALUES(%L)', [t_id, groupId, billId, payer, member, userAmount]);
                    await query(transactionInsertQuery);
                }
            }
        } else {
            const { amountSplit } = req.body;
            if (!amountSplit) {
                return res.status(403).json({ errors: [{ message: 'Please provide amount split' }] });
            }
            for (let member of members) {
                if (String(member) !== String(payer)) {
                    const t_id = v4();
                    const userAmount = amountSplit[member];
                    if (!userAmount) {
                        return res.status(403).json({ errors: [{ message: 'Please provide amount split for all users' }] });
                    }
                    const transactionInsertQuery = format('INSERT INTO transactions(id, group_id, bill_id, payer, payee, amount) VALUES(%L)', [t_id, groupId, billId, payer, member, userAmount]);
                    await query(transactionInsertQuery);
                }
            }
        }
        return res.status(200).send({ message: "Bill Successfully created", billId });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to create bill. Please try again later.' }] });
    }
});

//@route   POST /api/bills/createNewSettlement
//@desc    Create a new settlement
//access   Private

router.post('/createNewSettlement', auth, async (req, res) => {
    let { groupId, settlementAmountToPay, payingTo } = req.body;
    try {
        const t_id = v4();
        const transactionInsertQuery = format('INSERT INTO transactions(id, group_id, payer, payee, amount,t_type) VALUES(%L)', [t_id, groupId, req.id, payingTo, settlementAmountToPay, 'payment']);
        await query(transactionInsertQuery);
        return res.status(200).send({ message: "Payment Successfully created" });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to create bill. Please try again later.' }] });
    }
});

//@route   GET /api/bills/billdetails/:billid
//@desc    Fetch bill details
//access   Private

router.get('/billdetails/:billid', auth, async (req, res) => {
    const { billid } = req.params;
    try {
        let billDetails = {};
        const fetchBillDetailsQuery = format('SELECT u.name, b.amount FROM bills b JOIN users u ON b.payer = u.id');
        const { rows } = await query(fetchBillDetailsQuery);
        billDetails.payerName = rows[0].name;
        billDetails.amount = rows[0].amount;
        const fetchBillSplit = format('SELECT u.name, t.amount FROM transactions t JOIN users u ON t.payee = u.id WHERE t.bill_id = %L', billid);
        const { rows: billRows } = await query(fetchBillSplit);
        let splits = []
        for (let split of billRows) {
            const { name, amount } = split;
            splits.push({ name, amount });
        }
        billDetails.splits = splits;
        return res.status(200).send({ billDetails });
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to create bill. Please try again later.' }] });
    }
});

module.exports = router;