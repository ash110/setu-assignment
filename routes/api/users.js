const { v4 } = require('uuid');
const config = require('config');
const express = require('express');
const jwt = require('jsonwebtoken');
const format = require('pg-format');
const { query } = require('../../utils/postgres.js');

const router = express.Router();

//@route   POST /api/users/register
//@desc    Register a new user
//access   Public

router.post('/register', async (req, res) => {
    let { name, phone } = req.body;
    try {

        // Check if Name not blank
        if (!name?.trim()) {
            return res.status(403).json({ errors: [{ message: 'Name cannot be blank' }] });
        }

        // Check if Number is valid
        if (!phone?.trim() || phone.length != 10) {
            return res.status(403).json({ errors: [{ message: 'Phone Number has to be 10 digits' }] });
        }

        //Check if Email exists
        const checkPhoneQuery = format('SELECT id FROM users WHERE phone=%L', phone);
        const { rowCount } = await query(checkPhoneQuery);
        if (rowCount != 0) {
            return res.status(403).json({ errors: [{ message: 'An account has already been registered with this phone. Do you want to login instead?' }] });
        }

        const id = v4();

        // Save User to database
        const saveUserQuery = format('INSERT INTO users(id,name,phone) VALUES(%L)', [id, name, phone]);
        await query(saveUserQuery);

        const payload = {
            id,
            iat: new Date().getTime()
        }

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 3600 * 24 * 7 },
            (err, token) => {
                if (err) throw err;
                return res.status(200).json({ token });
            }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to Sign Up. Please try again later.' }] });
    }
});

//@route   POST /api/users/login
//@desc    Login user
//access   Public

router.post('/login', async (req, res) => {
    let { phone } = req.body;
    try {

        // Check if Number is valid
        if (!phone?.trim() || phone.length != 10) {
            return res.status(403).json({ errors: [{ message: 'Phone Number has to be 10 digits' }] });
        }

        //Check if account exists
        const checkPhoneQuery = format('SELECT id FROM users WHERE phone=%L', phone);
        const { rows, rowCount } = await query(checkPhoneQuery);
        if (rowCount != 1) {
            return res.status(403).json({ errors: [{ message: 'No account exists with this number. Please check again.' }] });
        }

        const id = rows[0].id;

        const payload = {
            id,
            iat: new Date().getTime()
        }

        jwt.sign(
            payload,
            config.get('jwtSecret'),
            { expiresIn: 3600 * 24 * 7 },
            (err, token) => {
                if (err) throw err;
                return res.status(200).json({ token });
            }
        )
    } catch (err) {
        console.log(err);
        return res.status(500).json({ errors: [{ message: 'Server Error. Failed to Login. Please try again later.' }] });
    }
});

module.exports = router;
