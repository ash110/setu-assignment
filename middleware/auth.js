const jwt = require('jsonwebtoken');
const config = require('config');

const auth = (req, res, next) => {
    //Get the token from the header
    //Check if token exists

    const token = req.header('x-auth-token');
    try {
        const decoded = jwt.verify(token, config.get('jwtSecret'));
        req.id = decoded.id;
        next();
    } catch (err) {
        res.status(401).json({ errors: ["Token is not valid"] });
    }
}


module.exports = auth;