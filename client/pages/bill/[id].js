import React, { useState, useEffect } from 'react';
import { useRouter } from 'next/router';
import axios from 'axios';

const Bill = () => {
    const router = useRouter();
    const [billId, setBillId] = useState('')
    const [billDetails, setBillDetails] = useState({})
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        setTimeout(() => {
            setBillId(router.query.id);
        }, 1000);
        axios.get(`/bills/billdetails/${router.query.id}`, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                setBillDetails(res.data.billDetails || {})
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }, [router.query.id])

    if (!loaded) {
        return <p className="m-20">Loading Bill Details</p>
    }

    return (
        <div className="flex flex-col p-20">
            <p className="text-3xl font-bold">
                Rs.{billDetails.amount}
            </p>
            <p className="text-xl mt-2">
                Paid By {billDetails.payerName}
            </p>
            <p className="text mt-10">
                Individual Split
            </p>
            {
                billDetails.splits.map((split, index) => {
                    return (
                        <div className="m-2 p-2 rounded bg-gray-50 w-1/3" key={index}>
                            {split.name}  : Rs.{split.amount}
                        </div>
                    )
                })
            }
        </div>
    );
}

export default Bill;