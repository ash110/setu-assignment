import React, { useEffect, useState } from 'react'
import Home from './home';

const Landing = () => {
  const [token, setToken] = useState('')
  useEffect(() => {
    setToken(window.localStorage.getItem('token'));
  }, [])
  if (token) {
    return <Home />
  }
  return (
    <div className="w-full flex items-center flex-col">
      <p className="text-3xl mt-20">SETU Splitwise</p>
      <button className="shadow-none border-none rounded bg-red-500 text-white py-2 px-16 mt-20" onClick={() => window.location.href = "/register"}>Register</button>
      <button className="shadow-none rounded text-red-500 border border-red-500 py-2 px-16 mt-8" onClick={() => window.location.href = "/login"}>Login</button>
    </div>
  );
}

export default Landing;