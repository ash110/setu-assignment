import axios from 'axios';
import React, { useState, useEffect } from 'react';
import Link from 'next/link';

const Home = () => {

    const [loaded, setLoaded] = useState(false);
    const [groups, setGroups] = useState([])
    const [newGroupName, setNewGroupName] = useState('')

    useEffect(() => {
        axios.get('/groups/fetchUserGroups', { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                setGroups(res.data.groups);
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }, []);

    if (!loaded) {
        return <p className="m-20">Loading user groups</p>
    }

    const createNewGroup = () => {
        setLoaded(false);
        axios.post('/groups/createGroup', { name: newGroupName }, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                let newGroups = [...groups];
                newGroups.push({ id: res.data.groupId, name: newGroupName });
                setGroups(newGroups);
            })
            .catch((err) => {
                console.log(err);
                for (let error of err.response.data.errors) {
                    window.alert(error.message);
                }
            })
            .finally(() => setLoaded(true));
    }

    return (
        <div className="p-16">
            {
                groups.length ?
                    groups.map((group) => {
                        return (
                            <Link href={`/group/${group.id}`} key={group.id} passHref>
                                <div className="cursor-pointer p-8 hover:shadow m-2 rounded border-b flex justify-between" >
                                    <p className="font-bold">{group.name}</p>
                                    <p>Click To Open</p>
                                </div>
                            </Link>
                        )
                    })
                    :
                    <p>You have no groups</p>
            }
            <div className="mt-20 flex flex-col w-1/3">
                <input type="text" placeholder="Enter New Group Name" className="shadow-none border rounded p-2 mt-8" onChange={(e) => setNewGroupName(e.target.value)} value={newGroupName} />
                <button className="mt-2 text-white bg-red-500 rounded py-2 px-10" onClick={createNewGroup}>
                    Create New Group
                </button>
            </div>
        </div>
    );
}

export default Home;