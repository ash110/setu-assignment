import axios from 'axios';
import { useRouter } from 'next/router';
import React, { useState, useEffect, Fragment, useRef } from 'react';
import { Dialog, Transition, Menu } from '@headlessui/react'
import { ExclamationIcon } from '@heroicons/react/outline'
import { ChevronDownIcon } from '@heroicons/react/solid'
import Link from 'next/link';

const Group = () => {

    const router = useRouter();

    const [loaded, setLoaded] = useState(false);
    const [bills, setBills] = useState([]);
    const [owingDetails, setOwingData] = useState([]);
    const [members, setMembers] = useState([]);
    const [newMembers, setNewMembers] = useState('');
    const [groupId, setGroupId] = useState('')
    const [open, setOpen] = useState(false)
    const [settlementOpen, setSettlementOpen] = useState(false)
    const [settlementId, setSettlementId] = useState('')
    const [settlementAmount, setSettlementAmount] = useState(0)
    const [settlementAmountToPay, setSettlementAmountToPay] = useState(0)

    // State for new bill
    const [newBillAmount, setNewBillAmount] = useState(0)
    const [split, setSplit] = useState('equal')
    const [payer, setPayer] = useState('')
    const [billSplits, setBillSplits] = useState({})

    useEffect(() => {
        setTimeout(() => {
            setGroupId(router.query.id);
        }, 1000)
        axios.get(`/groups/loadGroupInfo/${router.query.id}`, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                setBills(res.data.bills || [])
                setOwingData(res.data.owingDetails || [])
                setMembers(res.data.members || [])
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }, [router.query.id])

    if (!loaded) {
        return <p className="m-20">Loading Group Details</p>
    }

    const addNewMembers = () => {
        setLoaded(false)
        if (!newMembers) {
            window.alert("Enter one or more phone numbers");
        }
        const newMembersArray = newMembers.split(',');
        axios.post(`/groups/addMembersToGroup`, { members: newMembersArray, groupId }, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                const { newMembersDetails } = res.data;
                let newMembersList = [...members];
                for (let newMembersDetail of newMembersDetails) {
                    newMembersList.push(newMembersDetail);
                }
                setMembers(newMembersList);
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }

    const createNewBill = () => {
        setLoaded(false);
        let memberIds = [];
        for (let member of members) {
            memberIds.push(member.id);
        }
        axios.post(`/bills/createBill`, { groupId, amount: newBillAmount, split, members: memberIds, payer: payer.id, amountSplit: billSplits }, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                const { billId } = res.data;
                let newBills = [...bills];
                newBills.push({ id: billId, payer: payer.name })
                setBills(newBills);
                setOpen(false);
                window.alert("Bill Created. Refresh page to see updated settlements");
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }

    const settleAmount = (id, amount) => {
        setSettlementOpen(true);
        setSettlementAmount(amount);
        setSettlementAmountToPay(amount);
        setSettlementId(id);
    }

    const createNewSettlement = () => {
        setLoaded(false);
        if (settlementAmountToPay > settlementAmount || settlementAmountToPay <= 0) {
            window.alert("Amount needs to be between 1 and " + settlementAmount);
        }
        axios.post(`/bills/createNewSettlement`, { groupId, settlementAmountToPay, payingTo: settlementId }, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                window.alert("Payment Created. Refresh page to see updated settlements");
                setSettlementOpen(false);
            })
            .catch((err) => {
                console.log(err);
                if (err.response?.data?.errors) {
                    for (let error of err.response?.data?.errors) {
                        window.alert(error.message);
                    }
                }
            })
            .finally(() => setLoaded(true));
    }

    return (
        <Fragment>

            {/* MODAL FOR NEW BILL */}

            <Transition.Root show={open} as={Fragment}>
                <Dialog as="div" className="fixed z-10 inset-0" onClose={setOpen}>
                    <div className="flex items-end justify-center pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                        </Transition.Child>

                        {/* This element is to trick the browser into centering the modal contents. */}
                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                            &#8203;
                        </span>
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <div className="inline-block align-bottom bg-white rounded-lg text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                                <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                    <div className="sm:flex sm:items-start">
                                        <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                                            <ExclamationIcon className="h-6 w-6 text-red-600" aria-hidden="true" />
                                        </div>
                                        <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                            <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                                                Add New Bill
                                            </Dialog.Title>
                                            <div className="mt-2 flex flex-col p-2">
                                                Bill Amount
                                                <input type="number" placeholder="Enter bill amount" className="shadow-none border rounded p-2 mt-1" onChange={(e) => setNewBillAmount(e.target.value)} value={newBillAmount} />
                                                <br />
                                                Split Type
                                                <div className="flex">
                                                    <div className={`p-2 m-2 cursor-pointer rounded ${split === 'equal' ? 'bg-red-200' : 'bg-gray-100'}`} onClick={() => setSplit('equal')}>
                                                        Equal
                                                    </div>
                                                    <div className={`p-2 m-2 cursor-pointer rounded ${split === 'different' ? 'bg-red-200' : 'bg-gray-100'}`} onClick={() => setSplit('different')}>
                                                        Different Amounts
                                                    </div>
                                                </div>
                                                {
                                                    split === 'different'
                                                        ?
                                                        <div>
                                                            Enter amounts for each member
                                                            {
                                                                members.map((member) => {
                                                                    return (
                                                                        <div className="flex p-2 m-1 items-center" key={member.id}>
                                                                            <p>
                                                                                {member.name} :
                                                                            </p>
                                                                            <input type="number" placeholder="Enter amount" className="shadow-none border rounded p-2 ml-1" onChange={(e) => {
                                                                                let newBillSplits = { ...billSplits }
                                                                                newBillSplits[member.id] = e.target.value
                                                                                setBillSplits(newBillSplits);
                                                                            }} value={billSplits[member.id] || ''} />
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </div>
                                                        :
                                                        <div></div>
                                                }
                                                <br />
                                                Choose Payer
                                                <Menu as="div" className="relative inline-block text-left">
                                                    <div>
                                                        <Menu.Button className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-100 focus:ring-indigo-500">
                                                            {payer ? payer.name : 'Choose Payer'}
                                                            <ChevronDownIcon className="-mr-1 ml-2 h-5 w-5" aria-hidden="true" />
                                                        </Menu.Button>
                                                    </div>

                                                    <Transition
                                                        as={Fragment}
                                                        enter="transition ease-out duration-100"
                                                        enterFrom="transform opacity-0 scale-95"
                                                        enterTo="transform opacity-100 scale-100"
                                                        leave="transition ease-in duration-75"
                                                        leaveFrom="transform opacity-100 scale-100"
                                                        leaveTo="transform opacity-0 scale-95"
                                                    >
                                                        <Menu.Items className="origin-top-right absolute right-0 mt-2 w-56 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 focus:outline-none">
                                                            <div className="py-1">
                                                                {
                                                                    members.map((member) => {
                                                                        return (
                                                                            <Menu.Item key={member.id} onClick={() => setPayer(member)}>
                                                                                {({ _ }) => (
                                                                                    <a
                                                                                        href="#"
                                                                                        className={
                                                                                            payer.id === member.id ? 'bg-gray-100 text-gray-900' : 'text-gray-700',
                                                                                            'block px-4 py-2 text-sm'
                                                                                        }
                                                                                    >
                                                                                        {member.name}
                                                                                    </a>
                                                                                )}
                                                                            </Menu.Item>
                                                                        )
                                                                    })
                                                                }
                                                            </div>
                                                        </Menu.Items>
                                                    </Transition>
                                                </Menu>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <button
                                        type="button"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2  bg-green-300 hover:bg-green-600 text-base font-medium text-red focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={createNewBill}
                                    >
                                        Submit
                                    </button>
                                    <button
                                        type="button"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2  text-base font-medium text-red focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={() => setOpen(false)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </Transition.Child>
                    </div>
                </Dialog>
            </Transition.Root>

            {/* MODAL FOR NEW SETTLEMENT */}

            <Transition.Root show={settlementOpen} as={Fragment}>
                <Dialog as="div" className="fixed z-10 inset-0" onClose={() => setSettlementOpen(false)}>
                    <div className="flex items-end justify-center pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0"
                            enterTo="opacity-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                        </Transition.Child>

                        {/* This element is to trick the browser into centering the modal contents. */}
                        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                            &#8203;
                        </span>
                        <Transition.Child
                            as={Fragment}
                            enter="ease-out duration-300"
                            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                            enterTo="opacity-100 translate-y-0 sm:scale-100"
                            leave="ease-in duration-200"
                            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        >
                            <div className="inline-block align-bottom bg-white rounded-lg text-left shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">
                                <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                    <div className="sm:flex sm:items-start">
                                        <div className="mx-auto flex-shrink-0 flex items-center justify-center h-12 w-12 rounded-full bg-red-100 sm:mx-0 sm:h-10 sm:w-10">
                                            <ExclamationIcon className="h-6 w-6 text-red-600" aria-hidden="true" />
                                        </div>
                                        <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                            <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                                                Settle Payments
                                            </Dialog.Title>
                                            <div className="mt-2 flex flex-col p-2">
                                                Enter the amount you want to settle
                                                <input type="number" max={settlementAmount} placeholder="Enter amount to pay" className="shadow-none border rounded p-2 mt-8" onChange={(e) => setSettlementAmountToPay(e.target.value)} value={settlementAmountToPay} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <button
                                        type="button"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2  bg-green-300 hover:bg-green-600 text-base font-medium text-red focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={createNewSettlement}
                                    >
                                        Submit
                                    </button>
                                    <button
                                        type="button"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2  text-base font-medium text-red focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={() => setSettlementOpen(false)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </Transition.Child>
                    </div>
                </Dialog>
            </Transition.Root>

            <div className="flex w-full">
                <div className="flex w-3/4 flex-col">
                    <div className="m-2 p-2 rounded">
                        <p className="font-bold">Your Settlements</p>
                        {
                            owingDetails.length ?
                                owingDetails.map((data) => {
                                    if (data.type === 'owe') {
                                        return (
                                            <div className="flex">
                                                <p className="text-red-600 m-2">You owe {data.name} Rs.{Math.round(data.amount)}</p>
                                                <button className="bg-red-600 text-white rounded p-1" onClick={() => settleAmount(data.id, data.amount)}>
                                                    Settle Amount
                                                </button>
                                            </div>
                                        )
                                    }
                                    else if (data.type === 'owed') {
                                        return (
                                            <p className="text-green-600 m-2">You&aposre owed Rs.{Math.round(data.amount)} by {data.name}</p>
                                        )
                                    }
                                })
                                :
                                <p>You have no settlements</p>
                        }
                    </div>
                    <div className="mt-12 m-2 p-2">
                        <p className="font-bold">Group Bills</p>
                        <p className="text-sm mt-1 mb-4">Click on a bill to view split</p>
                        {bills.length ?
                            bills.map((bill, index) => {
                                return (
                                    <Link href={`/bill/${bill.id}`} key={index} passHref>
                                        <div className="bg-gray-50 w-1/2 flex flex-col p-4 rounded mt-2 hover:shadow cursor-pointer">
                                            <p className="font-bold">Bill #{index + 1} - Rs.{bill.amount}</p>
                                            <p className="mt-1">Paid By {bill.payer}</p>
                                        </div>
                                    </Link>
                                )
                            })
                            :
                            <p>Group has no bills</p>
                        }
                    </div>
                    <button className="mt-20 m-2 text-white bg-red-500 rounded py-2 px-10 w-48" onClick={() => setOpen(true)}>
                        Add Bill
                    </button>
                </div>
                <div className="flex w-1/4 p-2 flex-col ">
                    <p className="text-xl font-bold">Group Members</p>
                    <div className="m-2">
                        {
                            members.map((member) => {
                                return (
                                    <div className="p-2 bg-gray-50 rounded w-44 m-1" key={member.name}>
                                        {member.name}
                                    </div>
                                )
                            })
                        }
                    </div>
                    <div className="mt-20 flex flex-col">
                        <p>Enter Phone numbers of members to add, seperated by comma</p>
                        <input type="text" placeholder="Enter phone numbers" className="shadow-none border rounded p-2 mt-8" onChange={(e) => setNewMembers(e.target.value)} value={newMembers} />
                        <button className="mt-2 text-white bg-red-500 rounded py-2 px-10" onClick={addNewMembers}>
                            Add Members
                        </button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Group;