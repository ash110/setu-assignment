import '../styles/globals.css'
import Head from 'next/head';
import 'tailwindcss/tailwind.css'
import { Fragment } from 'react';
import axios from 'axios';

axios.defaults.baseURL = 'http://localhost:5000/api';
axios.defaults.headers.post['Content-Type'] = 'application/json';

function MyApp({ Component, pageProps }) {
	return (
		<Fragment>
			<Head>
				<title>Setu Assignment</title>
			</Head>
			<Component {...pageProps} />
		</Fragment>
	)
}

export default MyApp
