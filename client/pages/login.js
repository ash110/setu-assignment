import axios from 'axios';
import React, { useState } from 'react';

const Login = () => {
    const [phone, setPhone] = useState('')

    const login = () => {
        if (!phone) {
            window.alert("All details are required");
        }
        axios.post('/users/login', { phone }, { headers: { 'x-auth-token': window.localStorage.getItem('token') } })
            .then((res) => {
                window.localStorage.setItem('token', res.data.token);
                window.location.href = "/"
            })
            .catch((err) => {
                console.log(err);
                for (let error of err.response.data.errors) {
                    window.alert(error.message);
                }
            })
    }

    return (
        <div>
            <div className="w-full flex items-center flex-col">
                <p className="text-3xl mt-20">SETU Splitwise</p>
                <input type="text" placeholder="Enter Phone Number" className="shadow-none border rounded p-2 mt-8" onChange={(e) => setPhone(e.target.value)} value={phone} />
                <button className="shadow-none border-none rounded bg-red-500 text-white py-2 px-16 mt-20" onClick={login}>Login</button>
            </div>
        </div>
    );
}

export default Login;