# Splitwise - Setu Assignment

## Design Assumptions and drawbacks

- Transactions have not been used for SQL queries. In case of endpoints with multiple SQL inserts, if one fails, there is no rollback
- All users added to a group must be existing users in the users table
- When splitting a bill, all members of group must be involved. Wouldn't be hard to make it optional members, just a time constraint
- None of the frontend code is documented, in case you want to have a quick overview of that

## Instructions to run

- To run the entire project, run `make run`
- This will create docker containers for the database,server and client
- Open client on [port 3000](http://localhost:3000)
- Server runs on [port 5000](http://localhost:5000)
- To clean up the containers, run `make clean`

## Postman Collection

[Link to postman collection](https://www.getpostman.com/collections/7d2c8b5acbe7281c2c92)

When running the postman collection, after a Register or Login call, use the token in the response to update the JWT in the pre-request script for endpoints that need to be authenticated

## Endpoints

- /api/users
  - POST /register - Register a new user
  - POST /login - Login user

- /api/groups
  - POST /createGroup - Create a new group
  - POST /addMembersToGroup - Add members to group
  - GET /loadGroupMembers/:groupID - Get list of members of group
  - GET /loadGroupInfo/:groupID - Get group info like bills, who owes you, how much you owe etc.
  - GET /fetchUserGroups - Fetch all groups of the user

- /api/bills
  - POST /createBill - Create a bill with amount and split
  - POST /createNewSettlement - Create a settlement by paying some amount to someone you owe
  - GET /billdetails/:billID - Load details about a particular bill
