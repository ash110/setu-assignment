CREATE TYPE transaction_type as ENUM ('bill', 'payment');
CREATE TABLE IF NOT EXISTS users(
    id VARCHAR PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    phone VARCHAR(10) NOT NULL UNIQUE,
);
CREATE TABLE IF NOT EXISTS groups(
    id VARCHAR PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    creator_id VARCHAR,
    CONSTRAINT fk_creator FOREIGN KEY(creator_id) REFERENCES users(id)
);
CREATE TABLE IF NOT EXISTS group_members(
    group_id VARCHAR NOT NULL,
    member_id VARCHAR NOT NULL,
    CONSTRAINT fk_member FOREIGN KEY(member_id) REFERENCES users(id),
    PRIMARY KEY(group_id, member_id)
);
CREATE TABLE IF NOT EXISTS bills(
    id VARCHAR PRIMARY KEY,
    group_id VARCHAR NOT NULL,
    creator_id VARCHAR NOT NULL,
    amount DECIMAL NOT NULL,
    payer VARCHAR NOT NULL,
    CONSTRAINT fk_creator FOREIGN KEY(creator_id) REFERENCES users(id),
    CONSTRAINT fk_payer FOREIGN KEY(payer) REFERENCES users(id),
    CONSTRAINT fk_group FOREIGN KEY(group_id) REFERENCES groups(id)
);
CREATE TABLE IF NOT EXISTS transactions(
    id VARCHAR PRIMARY KEY,
    group_id VARCHAR NOT NULL,
    bill_id VARCHAR,
    payer VARCHAR NOT NULL,
    payee VARCHAR NOT NULL,
    amount DECIMAL NOT NULL,
    t_type transaction_type NOT NULL DEFAULT 'bill',
    CONSTRAINT fk_payer FOREIGN KEY(payer) REFERENCES users(id),
    CONSTRAINT fk_payee FOREIGN KEY(payee) REFERENCES users(id),
    CONSTRAINT fk_group FOREIGN KEY(group_id) REFERENCES groups(id),
    CONSTRAINT fk_bill FOREIGN KEY(bill_id) REFERENCES bills(id)
);