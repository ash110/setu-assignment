const config = require("config");
const { Pool } = require('pg')


const pool = new Pool({
    user: config.get('pguser'),
    host: config.get('pghost'),
    database: config.get('pgdatabase'),
    password: config.get('pgpassword'),
    port: config.get('pgport'),
});


module.exports = {
    query: (text, params) => pool.query(text, params),
}