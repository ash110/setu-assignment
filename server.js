const express = require('express');
const cors = require('cors');
const initDatabase = require('./helper/init_database');

const run = async () => {

    try {

        // Initialise Express and Middleware
        const app = express();
        app.use(express.json());
        app.use(cors());

        // Setup Routes
        app.use('/api/users', require('./routes/api/users'));
        app.use('/api/groups', require('./routes/api/groups'));
        app.use('/api/bills', require('./routes/api/bills'));

        // Connect to database
        await initDatabase();

        // Start the server
        const PORT = process.env.NODE_PORT || 5000;
        app.listen(PORT, () => console.log(`Server up on port ${PORT}`))

    } catch (err) {
        console.log(err);
    }

}

run()