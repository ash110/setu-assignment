pguser = setu
pgdatabase = setu
pgpassword = devpassword
pgport = 54321

setup_database:  ## Setup The database
	docker network create setu-network
	docker run -d \
    --name setu-postgres \
    -e POSTGRES_PASSWORD=$(pgpassword) \
    -e POSTGRES_USER=$(pguser) \
    -e POSTGRES_DB=$(pgdatabase) \
    -e PGDATA=/var/lib/postgresql/data/pgdata \
	-p $(pgport):5432 \
	--network="setu-network" \
    postgres
	docker run -d -p 8080:8080 --network="setu-network" --name setu-adminer adminer

setup_server:  ## build server dockerfile
	docker build . -t ash110/setu-assignment-server

setup_client: ## Build client dockerfile
	cd client
	docker build ./client/. -t ash110/setu-assignment-client

setup: setup_database setup_server setup_client
	

run: setup
	docker container run -d -p 5000:5000 --network="setu-network" --name setu-server ash110/setu-assignment-server
	docker container run -d -p 3000:3000 --network="setu-network" --name setu-client ash110/setu-assignment-client

clean:  ## Clean up the entire process
	docker container stop setu-postgres
	docker container rm setu-postgres
	docker container stop setu-adminer
	docker container rm setu-adminer
	docker container stop setu-server
	docker container rm setu-server
	docker container stop setu-client
	docker container rm setu-client
	docker network rm setu-network