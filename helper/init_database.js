const { query } = require("../utils/postgres");

const initDatabase = () => {
    return new Promise(async (resolve, reject) => {

        // Create Transaction Type ENUM
        try {
            await query(`
            DO $$ BEGIN
                CREATE TYPE transaction_type as ENUM ('bill', 'payment');
            EXCEPTION
                WHEN duplicate_object THEN null;
            END $$;
            `)
            console.log("ENUM transaction_type CREATED")

            // Setup tables
            await query("CREATE TABLE IF NOT EXISTS users(id VARCHAR PRIMARY KEY,name VARCHAR(100) NOT NULL,phone VARCHAR(10) NOT NULL UNIQUE);")
            console.log("TABLE users CREATED")
            await query("CREATE TABLE IF NOT EXISTS groups( id VARCHAR PRIMARY KEY, name VARCHAR(100) NOT NULL, creator_id VARCHAR, CONSTRAINT fk_creator FOREIGN KEY(creator_id) REFERENCES users(id));")
            console.log("TABLE groups CREATED")
            await query("CREATE TABLE IF NOT EXISTS group_members( group_id VARCHAR NOT NULL, member_id VARCHAR NOT NULL, CONSTRAINT fk_member FOREIGN KEY(member_id) REFERENCES users(id), PRIMARY KEY(group_id, member_id) );")
            console.log("TABLE group_members CREATED")
            await query("CREATE TABLE IF NOT EXISTS bills(id VARCHAR PRIMARY KEY, group_id VARCHAR NOT NULL, creator_id VARCHAR NOT NULL, amount DECIMAL NOT NULL, payer VARCHAR NOT NULL, CONSTRAINT fk_creator FOREIGN KEY(creator_id) REFERENCES users(id), CONSTRAINT fk_payer FOREIGN KEY(payer) REFERENCES users(id), CONSTRAINT fk_group FOREIGN KEY(group_id) REFERENCES groups(id)); ")
            console.log("TABLE bills CREATED")
            await query("CREATE TABLE IF NOT EXISTS transactions( id VARCHAR PRIMARY KEY, group_id VARCHAR NOT NULL, bill_id VARCHAR, payer VARCHAR NOT NULL, payee VARCHAR NOT NULL, amount DECIMAL NOT NULL, t_type transaction_type NOT NULL DEFAULT 'bill', CONSTRAINT fk_payer FOREIGN KEY(payer) REFERENCES users(id), CONSTRAINT fk_payee FOREIGN KEY(payee) REFERENCES users(id), CONSTRAINT fk_group FOREIGN KEY(group_id) REFERENCES groups(id), CONSTRAINT fk_bill FOREIGN KEY(bill_id) REFERENCES bills(id) );")
            console.log("TABLE transactions CREATED")
            resolve()
        } catch (err) {
            reject(err)
        }
    })
}

module.exports = initDatabase;